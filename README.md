# Bunch Brand

[Bunch Brand](http://bunchbrand.com/) is an automatic partnership network for the TikTok social media network.

## requirements

Bunch Brand follows the monorepo / serverless project structure [recommended by Zeit](https://zeit.co/blog/now-2). To run the application locally or deploy to production you must have [Zeit Now](https://zeit.co/docs) installed on your local machine.

## installation

1. clone this repo
2. `npm install`

## use

Bunch Brand uses the [Firebase Admin SDK](https://firebase.google.com/docs/admin/setup) on the backend. If you want to run the project locally you will need to create your own Firebase project and generate a [service account](https://firebase.google.com/docs/admin/setup?authuser=0#initialize_the_sdk).

After you have your service account file do the following:

1. Copy the contents of your service account file to your clipboard, then run:
2. `now secrets add @bunchbrand-service-account <service-account-contents-here>`
3. Add your service account details to the .env file too: `BUNCHBRAND_SERVICE_ACCOUNT=<service-account-contents-here>`

Now that your service account information is available to the application in we can it locally with `now dev`. If you would like to deploy it to production you can simply run `now`.
