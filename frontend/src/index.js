import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import "@fortawesome/fontawesome-free/js/all.js";

const Avatar = ({ src }) => (
  <div
    style={{
      backgroundImage: `url("${src}")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "50%",
      backgroundSize: "cover",
      borderRadius: "50%",
      overflow: "hidden",
      width: 24,
      height: 24,
      display: "inline-block",
      marginRight: 4
    }}
  />
);

function Index() {
  const [loading, setLoading] = useState(true);
  const [mention, setMention] = useState(null);
  const [url, setUrl] = useState(null);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);
  const [examples, setExamples] = useState({ mention: false, sharing: false });
  const [validating, setValidating] = useState(false);
  const avatars = [
    "https://p16.muscdn.com/img/musically-maliva-obj/1640521133762565~c5_100x100.jpeg",
    "https://p16.muscdn.com/img/musically-maliva-obj/1641719045213189~c5_100x100.jpeg",
    "https://p16.muscdn.com/img/musically-maliva-obj/1645148751206405~c5_100x100.jpeg",
    "https://p16.muscdn.com/img/musically-maliva-obj/1626581374815238~c5_100x100.jpeg",
    "https://p16.muscdn.com/img/musically-maliva-obj/1643448537274374~c5_100x100.jpeg",
    "https://p16.muscdn.com/img/musically-maliva-obj/1646034283494405~c5_100x100.jpeg",
    "https://p16.muscdn.com/img/musically-maliva-obj/1639718093955077~c5_100x100.jpeg",
    "https://p16.muscdn.com/img/musically-maliva-obj/1647320910781445~c5_100x100.jpeg",
    "https://p16.muscdn.com/img/musically-maliva-obj/1647092410292230~c5_100x100.jpeg",
    "https://p16.muscdn.com/img/musically-maliva-obj/1642863008154630~c5_100x100.jpeg"
  ];
  useEffect(() => {
    const getMentionee = async () => {
      if (!mention) {
        const response = await fetch("/api/select");
        const json = await response.json();
        setMention(json.mention);
      }

      if (loading) {
        setLoading(false);
      }
    };
    getMentionee();
  });

  if (loading) {
    return (
      <div className="container" style={{ paddingTop: "1rem" }}>
        <div className="text-center">
          <div className="spinner-grow spinner-grow-sm" />
        </div>
      </div>
    );
  }
  return (
    <div className="container" style={{ paddingTop: "1rem" }}>
      <h1>Grow your TikTok following</h1>
      <p>
        Join 100+ users who are growing their TikTok accounts for free by
        helping each other out 💪
      </p>
      <div style={{ marginBottom: "1rem" }}>
        {avatars.map(src => (
          <Avatar src={src} />
        ))}
      </div>
      <div className="form-group">
        <h3>Step 1.</h3>
        <p>Add the following shoutout to the end of your TikTok post:</p>
        <blockquote className="blockquote">
          shoutout to {mention} via @bunchbrand
        </blockquote>
        <small className="form-text text-muted">
          DO NOT COPY AND PASTE. Make sure you type out the whole shoutout as
          shown in the example below:
        </small>
        <div style={{ marginTop: "1rem" }}>
          {examples.mention ? (
            <iframe
              title="mention example"
              src="https://giphy.com/embed/eieNoJLEc4FlpFHZod"
              width="240"
              height="176"
              frameBorder="0"
              class="giphy-embed"
              allowFullScreen
            />
          ) : (
            <button
              type="button"
              className="btn btn-light"
              onClick={() => setExamples({ ...examples, mention: true })}
            >
              Show Example
            </button>
          )}
        </div>
      </div>
      <div className="form-group">
        <h3>Step 2.</h3>
        <p>
          Post your video to TikTok. Open the sharing menu on your video and
          click "Copy Link". Paste the link in the field below.
        </p>
        <input
          style={{ marginBottom: "1rem" }}
          type="url"
          className="form-control"
          placeholder="Your TikTok Video Link"
          value={url}
          onChange={e => setUrl(e.target.value)}
        />
        <p>
          <small className="form-text text-muted">
            Bunch Brand will validate your post to make sure the shoutout is
            correct.
          </small>
        </p>
      </div>
      <div className="form-group">
        <h3>Step 3.</h3>
        <p>
          Validate your post! If your validation is successful you will be added
          to the BunchBrand partner network.
        </p>
        <p>
          <small className="form-text text-muted">
            Each time you validate a post you will earn a mention by someone
            else in the partner network. You may only validate a post once a
            day.
          </small>
        </p>
        <button
          type="button"
          className="btn btn-primary"
          disabled={validating}
          onClick={async () => {
            await setValidating(true);

            const response = await fetch("/api/validate", {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify({
                mention,
                url
              })
            });
            const json = await response.json();
            if (json.error) {
              setError(json.error);
            } else {
              setSuccess(true);
            }

            setValidating(false);
          }}
        >
          VALIDATE
          {validating && <div className="spinner-grow spinner-grow-sm" />}
        </button>
        <small className="form-text text-danger">{error}</small>
        <small className="form-text text-success">
          {success && "Validation successful!"}
        </small>
      </div>
    </div>
  );
}

ReactDOM.render(<Index />, document.getElementById("root"));
