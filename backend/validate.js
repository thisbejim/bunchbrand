const fetch = require("node-fetch");
const cheerio = require("cheerio");
const admin = require("firebase-admin");

const serviceAccount = JSON.parse(process.env.BUNCHBRAND_SERVICE_ACCOUNT);

let app;

if (admin.apps.length) {
  app = admin.app();
} else {
  app = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://bunch-brand.firebaseio.com"
  });
}

const db = admin.database();

module.exports = async (req, res) => {
  const url = req.body.url;
  const expectedMention = req.body.mention;

  console.log(req.body);

  // check the url is correct
  if (!url.includes("vm.tiktok.com")) {
    res.statusCode = 400;
    return res.json({ error: "Post url is not correct." });
  }

  // check the mention exists
  if (!expectedMention) {
    res.statusCode = 400;
    return res.json({ error: "Mention is required." });
  }

  // the content expected to be found within the post text
  const expected = `shoutout to ${expectedMention} via @bunchbrand`;

  // fetch the post
  const response = await fetch(url);

  // get the post id
  const postUrl = response.url;
  const start = "/v/";
  const end = ".html";
  const id = postUrl.slice(
    postUrl.indexOf(start) + start.length,
    postUrl.indexOf(end)
  );

  // get the post username and text
  const html = await response.text();
  const $ = cheerio.load(html);
  const username = $(".userName")
    .text()
    .replace("<!-- -->", "");
  const text = $(".text").text();

  const usernameKey = username.split(".").join("");
  const mentionKey = expectedMention.split(".").join("");

  console.log(username, text);

  // check that the user being mentioned is not the poster
  if (username === expectedMention) {
    res.statusCode = 400;
    return res.json({ error: "User cannot mention themself." });
  }

  if (!text.toLowerCase().includes(expected)) {
    res.statusCode = 400;
    return res.json({ error: "Post does not contain the expected content." });
  }

  // check if this url has already been used
  const used = (await db.ref(`tiktok/used/${id}`).once("value")).val();

  if (used) {
    res.statusCode = 400;
    return res.json({ error: "This post has already been validated." });
  }

  // set this post as used
  await db.ref(`tiktok/used/${id}`).set(true);

  // get the user
  const user = (await db
    .ref(`tiktok/users/${usernameKey}`)
    .once("value")).val();

  if (user) {
    // check if the user is banned
    if (user.banned) {
      res.statusCode = 400;
      return res.json({ error: "This user is banned." });
    }
    // check that this user has not already posted today
    const millisecondsInADay = 86400000;
    if (new Date().getTime() - user.lastPosted < millisecondsInADay) {
      res.statusCode = 400;
      return res.json({ error: "Users can only post once a day." });
    }
  }

  // check that the user being mentioned exists and has points
  const mentioned = (await db
    .ref(`tiktok/users/${mentionKey}`)
    .once("value")).val();
  if (!mentioned || mentioned.points === 0) {
    res.statusCode = 400;
    return res.json({ error: "Mentioned user does not have any points left." });
  }

  if (user) {
    // update points and posting time for the user
    await db.ref(`tiktok/users/${usernameKey}`).update({
      points: user.points + 1,
      lastPosted: new Date().getTime()
    });
  } else {
    // if the user does not exist, create them
    await db.ref(`tiktok/users/${usernameKey}`).set({
      username,
      points: 1,
      lastPosted: new Date().getTime()
    });
  }

  // decrease points for the user mentioned
  await db.ref(`tiktok/users/${mentionKey}`).update({
    points: mentioned.points - 1
  });

  res.statusCode = 200;
  res.json({});
};
