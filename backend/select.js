const admin = require("firebase-admin");

const serviceAccount = JSON.parse(process.env.BUNCHBRAND_SERVICE_ACCOUNT);

let app;

if (admin.apps.length) {
  app = admin.app();
} else {
  app = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://bunch-brand.firebaseio.com"
  });
}

const db = admin.database();

module.exports = async (req, res) => {
  // get the first 10 users who have at least one point
  const limit = 10;
  const minPoints = 1;
  const users = Object.values(
    (await db
      .ref("tiktok/users")
      .orderByChild("points")
      .startAt(minPoints)
      .limitToFirst(limit)
      .once("value")).val()
  );
  // select a random user
  const mention = users[Math.floor(Math.random() * users.length)].username;
  res.statusCode = 200;
  res.json({ mention });
};
